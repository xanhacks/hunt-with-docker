# PHP x MariaDB

All-in-one docker-compose to audit PHP application.

## Getting started

1. Put your app inside the `app` folder.
2. Start docker compose with `docker-compose up`.

## Features

- XDebug with VSCode extension [PHP Debug](https://open-vsx.org/extension/felixfbecker/php-debug)
- composer
- PHP famous librairies

## MariaDB credentials

- MARIADB_DATABASE=database
- MARIADB_ROOT_PASSWORD=password
- MARIADB_USER=user
- MARIADB_PASSWORD=password

## PHP logs

- /var/logs/apache2/access.log
- /var/logs/apache2/error.log
- /var/logs/apache2/xdebug.log
